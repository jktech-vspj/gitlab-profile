# VŠPJ

## Popis
Mnou vypracovávané úkoly z oboru **Aplikovaná informatika** na VŠPJ. Úkoly používejte libovolně podle vlastního uvážení pro studium. V případě špatného řešení **nenesu žádnou zodpovědnost**. Pokud v nějakém cvičení narazíte na jakoukoli chybu nebo nefunkční odkazy, vytvořte u každého repositáře **issue**, chybu se pokusím odstranit. Předměty budu postupně po dobu studia přidávat. Některé předmětu (primárně ty nesouvisející s IT oborem) nebudu přidávat. 

## Předměty

### 1. Semestr (Zima 2022)
- [Základy strukturovaného programování](https://gitlab.com/jktech-vspj/zsp)
- [Tvorba internetových stránek](https://gitlab.com/jktech-vspj/tis)

### 2. Semestr (Léto 2023)
- [Architektura počítačů](https://gitlab.com/jktech-vspj/arp)
- [Návrh uživatelských rozhraní](https://gitlab.com/jktech-vspj/nur)
- [Objektově orientované programování](https://gitlab.com/jktech-vspj/oop)
- Ekonomika podniku
- Softwarové inženýrství
- Statistika pro techniky
- Úvod do databázových systémů

### 3. Semestr (Zima 2023)
- [Programování v javě](https://gitlab.com/jktech-vspj/pj)
- [Řízení softwarových projektů](https://gitlab.com/jktech-vspj/rsp)
- [Webové technologie](https://gitlab.com/jktech-vspj/wt)
- Datové struktury a algoritmy

### 4. Semestr (Léto 2024)
- Operační systémy
- Počítačová grafika a virtuální realita
- Podnikové informační systémy
- Programovací jazyky a překladače

### 6. Semestr
- [Pokročilé programovací techniky](https://gitlab.com/jktech-vspj/ppt)
- Programování pro mobilní platformy

## Další užitečné repozitáře
- [Lynder063](https://github.com/Lynder063?tab=repositories)

## Užitečné odkazy
- [Discord - DF VŠPJ](https://discord.gg/eBxeCjV5)
- [Discord - Aplikovaná Informatika](https://discord.gg/RvJddpsU)

